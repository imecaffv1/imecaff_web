from mongoengine import *
from mongo_db.db_configuration import DBNAME, _MONGODB_DATABASE_HOST
from mongo_db.db_documents_structures import *

# Making a connection with the DB using the configurations
connect(DBNAME, host=_MONGODB_DATABASE_HOST)

# method for adding information to the DB
# def add_data(collection, data):
#     if collection == 'banners':
#         data

# banner1 = Banners(
#     banner_id = 1,
#     banner_dynamic_content = 'content',
#     banner_dynamic_attr = '053-9d2840-f3491j'
# )
#
# banner2 = Banners(
#     banner_id = 2,
#     banner_dynamic_content = 'content_one',
#     banner_dynamic_attr = '053-9d2840-f3491'
# )
# banner1.save()
# banner2.save()

