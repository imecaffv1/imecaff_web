from mongoengine import *

"""
Design for all documents.

"""

class Projects(Document):
    project_id = IntField()
    project_type = StringField()
    project_user_id = IntField()
    project_number_of_searches = IntField()
    project_categories = ListField()
    project_searches = ListField()
    project_search_keyword = StringField()

class Banners(Document):
    banner_id = IntField()
    banner_img_src = URLField()
    banner_dynamic_attr = StringField()
    banner_dynamic_content = StringField()
    banner_categories = ListField()

class Users(Document):
    user_id = IntField()
    user_username = StringField()
    user_password = StringField()
    user_email = EmailField()
    user_credit = StringField()
    user_orders = ListField()
    user_type = StringField()
    user_is_admin = BooleanField()
    user_permmisions = StringField()
    user_categories = ListField()

class Searches(Document):
    search_id = IntField()
    search_url = URLField()
    search_alexa_ranking = IntField()
    search_banners = ListField()
    search_mailing_list = BooleanField()
    search_social_media = ListField()
    search_about_us = ListField()

class SocialNetworks(Document):
    social_network_id = IntField()
    social_network_name = StringField()
    network_offers = ListField()

class Categories(Document):
    category_id = IntField()
    category_name = StringField()
    category_sub_category = ListField()
    category_parent_category = ListField()

class Networks(Document):
    network_id = IntField()
    network_name = StringField()
    network_offers = ListField()

class Offers(Document):
    offer_id = IntField()
    offer_name = StringField()
    offer_landing_page = URLField()
    offer_categories = ListField()
    offer_content = StringField()
    offer_payout = StringField()
    offer_epc = StringField()

class Orders(Document):
    order_id = IntField()
    user_id = IntField()
    order_project_id = ListField()
    order_credit = ListField()
    order_start_date = DateTimeField()
    order_end_date = DateTimeField()
    order_invoice_id = IntField()

class Invoices(Document):
    invoice_id = IntField()
    invoice_name = StringField()
