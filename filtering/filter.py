from abc import ABCMeta, abstractmethod, abstractproperty
import os, json, tldextract
from urllib.parse import urlparse

class Filter(object):

    __metaclass__ = ABCMeta

    @abstractmethod
    def filter_actual_information(self, dict_to_filter, args_details):
        pass

class FilterBasement(Filter):

    def __init__(self):
         self._name = None

    @property
    def name(self):
        return self._name

    @name.setter
    @abstractmethod
    def name(self, value):
        pass

    @staticmethod
    def check_information_isactual(about_us_content, key_words):
        """

        :param about_us_content: list of about us information
        :type about_us_content: list
        :param key_words: list of key words to be in about us information
        :type key_words: list
        :return: boolean value if about us information is actual due to requests
        :rtype: bool
        """

        # check if about us information contains not less than one key word
        if about_us_content:
            splited_content = ' '.join(map(str, about_us_content)).lower()
            for element in key_words:
                if element in splited_content.split():
                    return True

    @staticmethod
    def get_filtering_keyword(customer_type):
        """

        :param customer_type: customer type information
        :type customer_type: str
        :return: list of keywords for selected customer type
        :rtype: list
        """

        # reading file to get all keywords for analyze about us information
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        with open(BASE_DIR + '/filtering/keywords_filtering.json') as file:
            file_content = json.loads(file.read(), encoding='utf8')
        return file_content[customer_type]

    @staticmethod
    def get_data_from_objects(obj_data, obj_link):
        """

        :param obj_data: dictionary with all extracted data
        :type obj_data: dict
        :param obj_link: link of an object
        :type obj_link: str
        :return: dictionary with object information (data)
        :rtype: dict
        """

        element = {}
        sorted_emails = []

        # reading an input dictionary and
        for key, value in obj_data.items():

            if key == 'banners':
                # element['banners'] = len(value)
                # element['banners'] = value
                banners_list = list(map(lambda b: b._banner_service, value))
                element['banners'] = {x:banners_list.count(x) for x in banners_list}
            elif key == 'emails':
                for email in value:
                    # getting a domain name and checking if an email is with this domain.
                    # If so, then add it to the begining, else - to the end
                    if tldextract.extract(obj_link).domain.lower() in email.lower():
                        sorted_emails.insert(0, email.lower())
                    else:
                        sorted_emails.append(email.lower())
                element['emails'] = sorted_emails
            else:
                element[key] = value

        return element

    @staticmethod
    def get_attributes_to_check(customer_type):

        # read the data from the file ( information about attributes to check for each customer type )
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        with open(BASE_DIR + '/filtering/attributes_filtering.json') as file:
            attributes_to_check = json.loads(file.read(), encoding='utf8')
        return attributes_to_check[customer_type]

