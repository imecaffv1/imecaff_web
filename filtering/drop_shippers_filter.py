from filtering import filter

class DropShippersFilter(filter.FilterBasement):

    def __init__(self):
        super(DropShippersFilter, self).__init__()
        self._name = "drop_shipper"

    def filter_actual_information(self, object_list, args_details):
        """

        :param object_list: list of objects after parcing
        :type object_list: list
        :return: dictionary with results for result.html page
        :rtype: dict
        """

        print('drop shippers filtering')
        result = {}
        key_words = filter.FilterBasement().get_filtering_keyword(self._name)
        attributes_to_check = filter.FilterBasement().get_attributes_to_check(self._name)

        for obj in object_list:
            obj_link = obj.domain.base_url
            actual_inf = filter.FilterBasement().check_information_isactual(obj.about_us.data, key_words)

            if all(map(lambda x: getattr(obj, x).data, attributes_to_check)) and actual_inf:
                obj_data = {}
                obj_data['rank'] = obj.domain._rank
                for argument in args_details:
                    obj_data[argument] = getattr(obj,argument).data
                result[obj.domain._base_url] = filter.FilterBasement().get_data_from_objects(obj_data, obj_link)

        return result
