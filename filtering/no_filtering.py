from filtering import filter

class NoFilter(filter.FilterBasement):

    def __init__(self):
        super(NoFilter, self).__init__()
        self._name = "No_filtering"

    def filter_actual_information(self, object_list, args_details):
        """

        :param object_list: list of objects after parcing
        :type object_list: list
        :return: dictionary with results for result.html page
        :rtype: dict
        """

        print('without filtering')
        result = {}

        for obj in object_list:

            obj_data = {}
            obj_data['rank'] = obj.domain._rank
            for argument in args_details:
                if getattr(obj,argument).data:
                    obj_data[argument] = getattr(obj,argument).data
                else:
                    obj_data[argument] = ''

            if obj.banners.data:
                # obj_data['banners'] = len(obj.banners.data)
                banners_list = list(map(lambda b: b._banner_service, obj.banners.data))
                obj_data['banners'] = {x: banners_list.count(x) for x in banners_list}
            else:
                obj_data['banners'] = []

            result[obj.domain._base_url] = obj_data

        return result