from filtering import filter

class CustomFilter(filter.FilterBasement):

    def __init__(self, custom_filtering_types):
        super(CustomFilter, self).__init__()
        self._name = "No_filtering"
        self.custom_filtering_types = custom_filtering_types

    def filter_actual_information(self, object_list, args_details):
        """

        :param object_list: list of objects after parcing
        :type object_list: list
        :return: dictionary with results for result.html page
        :rtype: dict
        """

        print('custom filtering')
        result = {}

        for obj in object_list:
            obj_link = obj.domain.base_url
            if all(map(lambda x: getattr(obj, x).data, self.custom_filtering_types)):
                obj_data = {}
                obj_data['rank'] = obj.domain._rank
                for argument in args_details:
                    obj_data[argument] = getattr(obj, argument).data
                result[obj.domain._base_url] = filter.FilterBasement().get_data_from_objects(obj_data, obj_link)

        return result