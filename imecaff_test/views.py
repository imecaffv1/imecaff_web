# -*- coding: utf-8 -*-
from django.shortcuts import render
import csv, sys, os, copy
from filtering import affiliates_filter, drop_shippers_filter, retailer_filter, amazon_filter, no_filtering, custom_filtering

def sample_view(request):
    current_user = request.user
    print('current user: ', current_user)

def main(request):
	sample_view(request)
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	market_codes = []

	# reading file with bing market codes
	with open(BASE_DIR + '/imecaff_test/static/bing_market_codes.csv') as file:
		mcodes = csv.DictReader(file, delimiter=';')
		for line in mcodes:
			market_codes.append({line['Country/Language'] : line['Market code']})

	# output dictionary for rendering main page
	args = {
		'name': 'IMECAFF',
		'market_codes': market_codes
	}

	return render(request, 'main.html', args)

def result(request):

	# changing working directory to get access to imecaff_logic folder
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	sys.path.append(os.path.join(BASE_DIR, 'imecaff_logic'))
	try:
		os.chdir(os.path.join(BASE_DIR, 'imecaff_logic'))
	except OSError:
		print("Can't change the Current Working Directory")
	from imecaff_logic import main as logic_main
	from csv_exctractor import extract_csv_raw_data, extract_csv_raw_data_for_pivot

	if request.method == 'POST':

		# getting data from request
		search_request = request.POST.get('search')
		market_code = request.POST.get('market-code')
		customer_type = request.POST.get('customer-type')
		filtering_type = request.POST.get('filtering-type')
		number_of_results = request.POST.get('number-of-results')
		custom_filtering_choose = ['banners', 'social_media','emails','mailing_list']
		custom_filtering_types = []

		# if custom filtering, then get users choice
		if filtering_type == 'custom':
			for x in custom_filtering_choose:
				if request.POST.get(x):
					custom_filtering_types.append(x)

		print('_________________________')
		print('REQUEST INFORMATION:')
		print('search_request:', search_request)
		print('market_code:', market_code)
		print('customer_type: ', customer_type)
		print('filtering-type: ', filtering_type)
		print('custom_filtering_types: ', custom_filtering_types)
		print('number of results: ', number_of_results)
		print('_________________________')

		# launching main parsing function
		all_objects = logic_main.main([
			'--k', search_request,
			'--mc', market_code,
			'--ct', customer_type,
			'--mp',
			'--sm',
			'--ml',
			'--em',
			'--al',
			'--ca',
			'--ba',
			'--au',
			# '--iframe_ba',
			'--nr', number_of_results
		])

		object_list = all_objects['affiliates']
		args_details = all_objects['args_details']
		failed_sites = all_objects['failed_sites']

		print('FAILED SITES: ', failed_sites)

		try:
			args_details.remove('about_us')
		except:
			pass

		# according to users choice apply a filtering method
		if filtering_type == 'default':
			if customer_type == 'affiliates':
				affiliates_json = affiliates_filter.AffFilter().filter_actual_information(object_list, args_details)
			elif customer_type == 'amazon':
				affiliates_json = amazon_filter.AmazonFilter().filter_actual_information(object_list, args_details)
			elif customer_type == 'retailer':
				affiliates_json = retailer_filter.RetailerFilter().filter_actual_information(object_list, args_details)
			elif customer_type == 'drop_shipper':
				affiliates_json = drop_shippers_filter.DropShippersFilter().filter_actual_information(object_list, args_details)
		else:
			if filtering_type == 'no-filtering':
				affiliates_json = no_filtering.NoFilter().filter_actual_information(object_list, args_details)
			elif filtering_type == 'custom':
				affiliates_json = custom_filtering.CustomFilter(custom_filtering_types).filter_actual_information(object_list, args_details)

		# creating a csv file
		try:
			csv_file_raw = extract_csv_raw_data(copy.deepcopy(affiliates_json))
			csv_file_pivot = extract_csv_raw_data_for_pivot(copy.deepcopy(affiliates_json))

		except SystemError:
			csv_file_raw = []
			csv_file_pivot = []

	else:

		# in case if POST request failed the output is empty
		affiliates_json = {}
		search_request = ''
		failed_sites = []
		csv_file_raw = []
		csv_file_pivot = []

	# output dictionary for rendering result page
	args = {
		'json_results': affiliates_json,
		'search_request': search_request,
		'failed_sites': failed_sites,
		'csv_file_raw': csv_file_raw,
		'csv_file_pivot': csv_file_pivot
	}

	return render(request, 'result.html', args)
