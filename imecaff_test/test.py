
# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import render
import json


def main(request):
	companyName = 'imecaff'
	json = {'fi': {'a':'a1', 'b':'b1'}, 'se': {'c':'c1','d':'d1'}}
	numbers = [1,2,3]
	args = {'name': companyName, 'json': json, 'numbers': numbers}
	return render(request, 'main.html', args)

def result():

	json_str = {
		"https://www.fxp.co.il/%D7%90%D7%95%D7%A4%D7%A0%D7%94_%D7%95%D7%94%D7%9C%D7%91%D7%A9%D7%94/tags/%D7%91%D7%92%D7%93%D7%99%D7%9D_%D7%9E%D7%97%D7%95%D7%9C/": "{\"_domain\": \"{\\\"_base_url\\\": \\\"https://www.fxp.co.il/%D7%90%D7%95%D7%A4%D7%A0%D7%94_%D7%95%D7%94%D7%9C%D7%91%D7%A9%D7%94/tags/%D7%91%D7%92%D7%93%D7%99%D7%9D_%D7%9E%D7%97%D7%95%D7%9C/\\\", \\\"_rank\\\": 1, \\\"_found_by_search_engine\\\": \\\"Bing\\\"}\", \"emails\": [], \"banners\": {\"doubleclick\": 1}, \"social_media\": [\"https://www.facebook.com/fxp.co.il\"], \"mailing_list\": true, \"alexa_ranking\": \"24911\"}",
		  "http://www.2net.co.il/shopping-hul.html": "{\"_domain\": \"{\\\"_base_url\\\": \\\"http://www.2net.co.il/shopping-hul.html\\\", \\\"_rank\\\": 2, \\\"_found_by_search_engine\\\": \\\"Bing\\\"}\", \"emails\": [], \"banners\": {\"aliexpress\": 17, \"jdoqocy\": 3, \"sharesale\": 2}, \"social_media\": [\"https://www.google.com/shopping\", \"https://www.pinterest.com/categories/home_decor/\"], \"mailing_list\": false, \"alexa_ranking\": \"91112\"}",
		  "https://www.themarker.com/consumer/1.1932882": "{\"_domain\": \"{\\\"_base_url\\\": \\\"https://www.themarker.com/consumer/1.1932882\\\", \\\"_rank\\\": 3, \\\"_found_by_search_engine\\\": \\\"Bing\\\"}\", \"emails\": [], \"banners\": {\"doubleclick\": 6}, \"social_media\": [\"http://www.facebook.com/TheMarkerOnline\", \"https://plus.google.com/100703308833620647697\", \"https://plus.google.com/100703308833620647697/posts\", \" @themarker\", \"https://twitter.com/themarker\", \"http://twitter.com/#!/TheMarker\", \"https://www.youtube.com/user/TheMarkerOnline\", \"https://www.youtube.com/iframe_api\", \"https://www.youtube.com/user/TheMarkerOnline \", \"https://www.facebook.com/TheMarkerOnline/\", \"http://www.youtube.com/user/TheMarkerOnline?\"], \"mailing_list\": true, \"alexa_ranking\": \"15778\"}",
		  "https://taxes.gov.il/customs/PersonalImport/Pages/yebueshe_guide.aspx": "{\"_domain\": \"{\\\"_base_url\\\": \\\"https://taxes.gov.il/customs/PersonalImport/Pages/yebueshe_guide.aspx\\\", \\\"_rank\\\": 4, \\\"_found_by_search_engine\\\": \\\"Bing\\\"}\", \"emails\": [], \"banners\": {}, \"social_media\": [\"https://www.facebook.com/taxesgovil\"], \"mailing_list\": false, \"alexa_ranking\": \"65176\"}",
		  "http://saloona.co.il/shirleyk7/?p=18": "{\"_domain\": \"{\\\"_base_url\\\": \\\"http://saloona.co.il/shirleyk7/?p=18\\\", \\\"_rank\\\": 5, \\\"_found_by_search_engine\\\": \\\"Bing\\\"}\", \"emails\": [], \"banners\": {\"doubleclick\": 6}, \"social_media\": [\"https://plus.google.com/103720309779896457696/posts\", \"https://www.youtube.com/user/saloonaIL\", \"https://www.facebook.com/saloonaweb\", \"http://twitter.com/SaloonaWeb\", \"http://feeds.feedburner.com/Saloona\", \"http://instagram.com/saloonaweb\"], \"mailing_list\": true, \"alexa_ranking\": \"102556\"}",
		  "http://www.yamyabasha.co.il/%D7%99%D7%91%D7%95%D7%90_%D7%91%D7%92%D7%93%D7%99%D7%9D": "{\"_domain\": \"{\\\"_base_url\\\": \\\"http://www.yamyabasha.co.il/%D7%99%D7%91%D7%95%D7%90_%D7%91%D7%92%D7%93%D7%99%D7%9D\\\", \\\"_rank\\\": 6, \\\"_found_by_search_engine\\\": \\\"Bing\\\"}\", \"emails\": [], \"banners\": {}, \"social_media\": [\"https://plus.google.com/112387804848496586572?rel=author\"], \"mailing_list\": false, \"alexa_ranking\": \"3160609\"}",
	}

	def get_keys(json_str):
		keys = []
		for main_key, main_value in json_str.items():
			for key, value in json.loads(main_value).items():
				keys.append(key)
		keys.append('_rank')
		keys.append('_found_by_search_engine')
		keys = list(set(keys))
		keys.remove('_domain')
		return keys

	def create_empty_json(keys):
		empty_dict = {}
		for key in keys: empty_dict[key] = {}
		return empty_dict

	def parse_json(json_str):
		output = {}

		for main_key, main_value in json_str.items():
			pattern = create_empty_json(get_keys(json_str))
			output[main_key] = pattern

			for key1, value1 in json.loads(main_value).items():

				if isinstance(value1, dict) & (key1 == 'banners'):
					pattern[key1] = value1

				elif isinstance(value1, list):
					if not value1:
						pattern[key1] = {}
					else:
						l = 0
						elements = {}
						for element in value1:
							elements[l] = element
							l =+ 1
						pattern[key1] = elements

				elif isinstance(value1, str):
					# value_u = value1.encode('ascii', 'ignore')
					if not value1:
						pattern[key1] = {}
					elif (key1 == 'alexa_ranking'):
						pattern[key1] = {key1: value1}
					else:
						for key_uni, value_uni in json.loads(value1).items():
							pattern[key_uni] = {key_uni: value_uni}

				else:
					if key1 == 'mailing_list':
						pattern[key1] = {key1: str(value1)}
		return output

	json_output = {'json_result': json.dumps(parse_json(json_str))}

	return json_output

json_result = {'json_result': result()}

print(json_result)
