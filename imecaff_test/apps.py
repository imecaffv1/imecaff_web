from django.apps import AppConfig


class ImecaffTestConfig(AppConfig):
    name = 'imecaff_test'
