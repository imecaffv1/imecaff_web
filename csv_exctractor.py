# coding=utf-8
import pandas as pd

"""
Method for downloading a csv file with all results.

Input data: json file
Output: csv formatted string

"""

def extract_csv_raw_data(json):

    if json:
        result = {}
        i = 0
        for key, value in json.items():
            print(key)
            reso = {}
            reso['link'] = key
            for k, v in value.items():
                if v == None:
                    json[key][k] = ''
                if isinstance(v, list):
                    if len(v) != 0:
                        res = ' '.join(map(str, v))
                        reso[k] = res
                    else:
                        reso[k] = ''
                else:
                    reso[k] = v

            result[i] = reso
            i+=1

        df = pd.DataFrame(result).T
        output = [df.columns.tolist()]
        for row in range(len(df.values)):
            output.append([','.join([str(x) for x in df.values[row]])])
    else:
        output = []
    return output

def extract_csv_raw_data_for_pivot(json):

    if json:
        for key, value in json.items():
            for k, v in value.items():
                if v == None:
                    json[key][k] = ''
        result = {}
        i = 0
        for key, value in json.items():
            len_dict = {
                # 'banners': len(value['banners']),
                'social_media': len(value['social_media']),
                'emails': len(value['emails'])
            }
            keys = value.keys()
            max_amount = max(len_dict.values())
            if max_amount == 0:
                res = value
                res['link'] = key
                result[str(i)] = res
                i+=1
            else:
                for el in range(0, max_amount):
                    res = {}
                    for k in keys:
                        try:
                            if k == 'emails':
                                res[k] = value[k][el]
                            elif k == 'social_media':
                                res[k] = value[k][el]
                            elif k == 'banners':
                                res[k] = value[k][el]
                            else:

                                res[k] = value[k]
                        except:
                                res[k] = ''
                    res['link'] = key
                    result[str(i)] = res
                    i+=1
        for key, value in result.items():
            for k, v in value.items():
                if isinstance(v, list) and len(v) == 0:
                    result[key][k] = ''
        df = pd.DataFrame(result).T
        output = [df.columns.tolist()]
        for row in range(len(df.values)):
            output.append([','.join([str(x) for x in df.values[row]])])
    else:
        output = []
    return output
