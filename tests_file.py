
from django.shortcuts import render
import json, sys, os, bs4, csv, io, copy
import tldextract

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.join(BASE_DIR, 'imecaff_updated/imecaff_logic'))
try: os.chdir(os.path.join(BASE_DIR, 'imecaff_updated/imecaff_logic'))
except OSError: print("Can't change the Current Working Directory")
from imecaff_logic import main as logic_main
from filtering import affiliates_filter, drop_shippers_filter, retailer_filter, amazon_filter, no_filtering, custom_filtering
from filtering import filter
from csv_exctractor import extract_csv_raw_data
import time

#
# all_objects = logic_main.main(['--k', 'three chicken',
#                                '--mp',
#                                '--sm',
#                                '--ml',
#                                # '--iframe_ba',
#                                '--al',
#                                '--em',
#                                '--ba',
#                                '--ct', 'no_filtering',
#                                '--mc', 'en-US',
#                                '--ca',
#                                '--au'])
# json_str = all_objects['affiliates_json']
# object_list = all_objects['affiliates']
# for each in object_list:
#     print(each.domain.base_url)
# filtering_type = 'custom'
# customer_type = 'affiliates'
# custom_filtering_types = ['emails']
# args_details = all_objects['args_details']
#
# if filtering_type == 'default':
#     if customer_type == 'affiliates':
#         affiliates_json = affiliates_filter.AffFilter().filter_actual_information(object_list, args_details)
#     elif customer_type == 'amazon':
#         affiliates_json = amazon_filter.AmazonFilter().filter_actual_information(object_list, args_details)
#     elif customer_type == 'retailer':
#         affiliates_json = retailer_filter.RetailerFilter().filter_actual_information(object_list, args_details)
#     elif customer_type == 'drop_shipper':
#         affiliates_json = drop_shippers_filter.DropShippersFilter().filter_actual_information(object_list, args_details)
# else:
#     if filtering_type == 'no-filtering':
#         affiliates_json = no_filtering.NoFilter().filter_actual_information(object_list, args_details)
#     elif filtering_type == 'custom':
#         affiliates_json = custom_filtering.CustomFilter(custom_filtering_types).filter_actual_information(object_list,
#                                                                                                           args_details)
#
# print(affiliates_json)

from urllib.parse import urlparse
# from urlparse import urlparse  # Python 2
parsed_uri = urlparse('http://www.stackoverflow.com/questions/1234567/blah-blah-blah-blah' )
u_domain = '{uri.netloc}'.format(uri=parsed_uri)
print(u_domain)

email = 'alex@stackoverflow.com'
e_domain = email[ email.find("@") + 1 : ]
print(e_domain)

emails = ['jasfn@gmail.com', 'lox@gmail.com', 'ya@ya.ru', 'mail@rambler.ru', 'wdoni@gmail.com']
link = 'http://gmail.com'

list = []
for i in emails:
    if '{uri.netloc}'.format(uri=urlparse(link)) in i:
        list.insert(0, i)
    else:
        list.append(i)

print(list)

url='https://www.hello.org/bye/'
print(tldextract.extract(link).domain.lower())